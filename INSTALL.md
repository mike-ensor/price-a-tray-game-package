# Overview

This project is the package bundle for the entire Price-a-Tray game on GDC. For a brief overview of the game and technology, please visit: [Modern AI Expereriences: Price-a-Tray YouTube Video](https://youtu.be/LoA_nMEtX3M?si=0S-XRTmmknEgExBp)

This doc references how to install and configure the game to run on your GDC cluster.

## Pre-Reqs
* GDC Cluster with `kubectl` access (minimum)
* GDC Cluster with ConfigSync setup and installed (preferred)
* 2 GCP Secret Manager secrets created (see below)
* Some SDS preferred, and knowledge of the `StorageClass` that will work for your cluster (GDC default = `robin`)

## Project Overview

This project uses Kustomize to hydrate the final manifests. The `base/kustomization.yaml` points to the other depenent applications, this project is the aggregate of those manifests into an "Application Package" for ConfigSync. Specific configuration (often called "variants") are configured in this project using "Overlays" (see: `overlays/<cluster-name>` for examples). This project does use static IPs from `LoadBalancer` as the end application is mostly browser based and needs to have routable references to most of the services. `Ingress` is possible, but requries private IPs to be added to DNS which is not a common practice. DNS would be a possiblity if the local DNS services can resolve IPs, Hosts or KubeDNS at the client/browser (out of scope for the rest of this doc).

## Fleet Resource Bundle

To generate the fleet resource bundle, set up `.envrc` or ENV vars then run the `envsubst` command to replace the variables

### Variables

| Variable             | Definition                              | Example                                      |
|----------------------|-----------------------------------------|----------------------------------------------|
| `${PROJECT_ID}`      | GCP Project                             | anthos-bare-metal-project                    |
| `${GSA_NAME}`        | GCP GSA for CloudBuild repo v2 access   | fp-gcb-gsa@<project>.iam.gserviceaccount.com |
| `${PACKAGE_NAME}`    | Application name                        | price-a-tray-package                         |
| `${PACKAGE_VERSION}` | Application version (git-tag)           | v0.0.1                                       |
| `${CONNECTION_NAME}` | Defined "Connection" name in CloudBuild | price-a-tray                                 |
| `${REPOSITORY_NAME}` | CloudBuild repository name              | price-a-tray-game-package                    |

```shell
envsubst < fleet-resource-bundle.yaml.template > fleet-resource-bundle.yaml
```

### Fleet Package Variant Variables
| Variable | Description | Pattern | File Name |
|----------|-------------|---------|-----------|
| `membership.name` | The name of the Fleet membership (often the cluster name) | app-config-${membership.name} | `app-config-gdc-pae1.yaml` |
| `membership.location` | The location of the cluster in the fleet membership | app-config-${membership.location} | `app-config-global.yaml` |
| `membership.labels[‘store’]` | The value of a cluster label | app-config-$membership.labels[‘store’] | Labe is `store=corporate` => `app-config-corporate.yaml` |

## How to use

Must apply the following code snippet. This code will create a new `RepoSync` object pointing to the Cluster Trait Repo, and create an `ExternalSecret` that allows the `RootSync` to reference a private repository.

> :note: This can be applied via ACM updates IF the Primary Root Repo is `unstructured`, otherwise a manual `kubectl apply -f <file.yaml>` is required to install this CTR.

```yaml
apiVersion: configsync.gke.io/v1beta1
kind: RepoSync
metadata:
  name: pat-pkg                                      # 7 characters
  namespace: price-a-tray
  annotations:
    configsync.gke.io/deletion-propagation-policy: Foreground
spec:
  sourceFormat: "unstructured"
  git:
    repo: "https://gitlab.com/mike-ensor/price-a-tray-game-package.git"
    branch: "main"
    dir: "/config/<CLUSTER_NAME>"                    # NOTE: Name of the cluster is required (and must be a varient)
    auth: "token"
    secretRef:
      name: price-a-tray-pkg-git-creds               # matches the ExternalSecret spec.target.name below

---

kind: RoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: pat-pkg-ksa-repo
  namespace: price-a-tray
subjects:
  - kind: ServiceAccount
    name: ns-reconciler-price-a-tray-pat-pkg-7      # k get sa -n config-management-system  ( ns-reconciler-{NAMESPACE}-{REPO_SYNC_NAME}-{REPO_SYNC_NAME_LENGTH} )
    namespace: config-management-system
roleRef:
  kind: Role
  name: reconciler-admin                            # Granular control over resources can be created with RBAC (Future demo will explore custom RBAC)
  apiGroup: rbac.authorization.k8s.io

---

# Role for Reconciler
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  namespace: price-a-tray
  name: reconciler-admin
rules:
- apiGroups: ["*"]
  resources: ["*"]
  verbs: ["*"]

---

apiVersion: external-secrets.io/v1beta1
kind: ExternalSecret
metadata:
  name: price-a-tray-pkg-git-creds-es
  namespace: config-management-system
spec:
  refreshInterval: 24h
  secretStoreRef:
    kind: ClusterSecretStore
    name: gcp-secret-store
  target:                                       # K8s secret definition
    name: price-a-tray-pkg-git-creds            ############# Matches the secretRef in the RepoSync object's authentication
    creationPolicy: Owner
  data:
  - secretKey: username                         # K8s secret key name inside secret
    remoteRef:
      key: price-a-tray-pkg-access-token-creds  #  GCP Secret Name
      property: username                        # field inside GCP Secret
  - secretKey: token                            # K8s secret key name inside secret
    remoteRef:
      key: price-a-tray-pkg-access-token-creds  #  GCP Secret Name
      property: token                           # field inside GCP Secret

```

### Create GCP Secret for git-creds

Create the GCP Secret Manager secret used by `ExternalSecret` to proxy for K8s `Secret`

```
export PROJECT_ID=<your google project id>
export SCM_TOKEN_TOKEN=<your gitlab personal-access token value>
export SCM TOKEN_USER=<your gitlab personal-access token user>

gcloud secrets create price-a-tray-pkg-access-token-creds --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"token\"{{':'}} \"${SCM_TOKEN_TOKEN}\", \"username\"{{':'}} \"${SCM_TOKEN_USER}\"}" | gcloud secrets versions add price-a-tray-pkg-access-token-creds --project="${PROJECT_ID}" --data-file=-
```

### Create Database Password for Postgres

Postgres needs a password to setup the database with. Randomly generate or use any password. Please do not use this pattern for production, this is for demonstration purpoases.


```
export PROJECT_ID=<your google project id>
export POSTGRES_PASSWORD=<pick a password and store in Google Secrets>

gcloud secrets create price-a-tray-postgres-password --replication-policy="automatic" --project="${PROJECT_ID}"
echo -n "{\"password\":\"${POSTGRES_PASSWORD}\"}" | gcloud secrets versions add price-a-tray-postgres-password --project="${PROJECT_ID}" --data-file=-
```

### Create KSA access to AI Models bucket

The `model-puller` uses Google Serice Account key as a Kubernetes secret to access the bucket with the models.  The secret is generated for you by the ExternalSecret `google-model-auth-external-secret` from the `price-a-tray-inference-servrer` project. Create a Google Cloud Secret with the contents of the GSA JSON key under a secret named `google-model-auth`. The corresponding secret in K8s should be called `google-model-auth`.


> :warning: NOTE: Use the below method IF you cannot use ExternalSecrets

1. Create a GSA with read-only access to the bucket containing the models
1. Create a GSA Key and save as `credentials.json`
1. Run the following command

```
kubectl create secret generic google-model-auth -n price-a-tray --from-file=./credentials.json
```

## Local Validation

Assuming `nomos` is installed (via `gcloud components install nomos`)

```
nomos vet --no-api-server-check --path config/
```

### Docker method

Using this link to find the version of nomos-docker:  https://cloud.google.com/anthos-config-management/docs/how-to/updating-private-registry#expandable-1

```
docker pull gcr.io/config-management-release/nomos:stable
docker run -it -v $(pwd):/code/ gcr.io/config-management-release/nomos:stable nomos vet --no-api-server-check --path /code/config/
```
