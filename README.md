# Overview

Price-a-Tray is a GDC Connected demonstration highlighting several of the fundmental reasons to use Edge. The game is designed to be highly interactive, players will be adding, moving and removing items off of the tray during the 30 second game play.

Training of the model datasets is described with detail in this blog post: https://medium.com/@mike-ensor/ai-edge-a-scalable-approach-d5878fe75d67

## Game Play

The game starts by explaining the landscape. The player steps up to the tray and is presented with multiple items. Items are dependent on the data set configured to use, but are often 15-25 items.

The goal of the game is to put a combintion of items onto the tray matching as close to the "target" as possible. The tray can be over or under the target price, The score is the absolute value of the difference, so if the target is $10.00, and the player has $10.10 or $9.90, the score is $0.10.  The game plays for 30 seconds, so move fast. The prices of the items are not shown to the player, so they need to experiment as they place items and remove them from the tray.

The game starts by pressing "`p`" or clicking on "`start`". The background will turn green for 3 seconds giving a visual indication that the time is running. At 5 seconds, the background starts to turn red until time runs out. If the player makes a score in the top 10, then they are offered an optional prompt to save their name or initials.

## Deploying

To deploy this application, [follow the INSTALL.md](INSTALL.md) instructions for a more in-depth description.

Ensure you have purchased the correct and exact dataset items if you plan on re-using or extending an existing model. Additionaly, there are multiple items that are necessary to setup and play the game. Follow the [instructions and details](docs/BILL_OF_MATERIALS.md) page for more information.

**NOTE**, the Models must be in an accessible GCS bucket for the application to pull from. Follow the appraoch in [this blog post](https://medium.com/@mike-ensor/ai-edge-a-scalable-approach-d5878fe75d67) and/or this [helper repository](https://gitlab.com/mike-ensor/vertex-ai-computer-vision-helper) to setup the GCS bucket structure and build your own models.

- [ ] Setup environment (ie: local machine or CI/CD)
  - [ ] Add `kustomize`
  - [ ] Clone this repository
- [ ] Create Secrets for Postgres Database <sup>&dagger;</sup>
- [ ] Create Credentials secret for Google Cloud Storage (GCS) bucket where the ML Model(s) exist <sup>&dagger;</sup>
- [ ] Optionally (though recommended) [install ConfigSync](https://cloud.google.com/kubernetes-engine/enterprise/config-sync/docs/how-to/installing-config-sync)
- [ ] Create an "Overlay" (ie: `overlays/your-cluster-name`) and modify patched values as needed
- [ ] Run `./generate.sh` to produce each variant
- [ ] Ensure [items necessary to run the game](docs/BILL_OF_MATERIALS.md) have been acquired and setup

> <sup>&dagger;</sup> Either a K8s `Secret` or GCP Secret Manager after installing [https://external-secrets.io](https://external-secrets.io) which is highly recommended


### ConfigSync Overview

This is a "package" repository, which in short means it is a combination of multiple smaller projects that compose to make a single application. Deploying this repository will automatically deploy the dependent projects listed below.

See [our documentation](https://cloud.google.com/kubernetes-engine/enterprise/config-sync/docs/overview) for how to use each subdirectory.

## Series Links

This project is one component in a multi-component solution. The composed parts make up the game "Price a Tray".

https://gitlab.com/mike-ensor/price-a-tray-dynamic-frontend
: This is the User Experience and User Interface for the game

https://gitlab.com/mike-ensor/price-a-tray-backend
: This is the Game API where rules, game lifecycle, scores and data are stored

https://gitlab.com/mike-ensor/rtsp-to-mjpeg
: This project turns a RTSP stream into a MJPEG stream for consumption by the UI

https://gitlab.com/mike-ensor/price-a-tray-game-package
: This is the project used to deploy to a Kubernetes project with ConfigSync installed

https://gitlab.com/mike-ensor/price-a-tray-inference-server
: This project provides an API to inference extraced frames (image) against the edge model pulled from the GCS bucket.

https://gitlab.com/mike-ensor/vertex-ai-computer-vision-helper
: Process and helper scripts to train models using a MLOps pipeline and export models for use at the edge