# Overview

Playing the game requires a physical data set. Mulitple physical datasets exist with more coming over time. If the desire is to play OOTB with minimal effort, please use the below list of materials to run the game locally.

## Infrastructure & Operational Items
- [ ] Camera - Needs to be an RTSP (Real Time Streaming Protocol) camera producing `1024 × 576` to `1280 x 720` resolution. Pre-trained models are trained using 16:9 ratio. Larger resolution is possible, but will require a fairly performant [game computer](#game-computer). **Barrel Control** is a REQUIRED feature to flaten out fisheye lenses.
    * [Axis P3265](https://amzn.to/3CyEoXu) - Camera used at all conferences & demos
    * [Unif G5 Turret](https://amzn.to/4hUtvzy) - Easy integration with Unifi Networking
- [ ] Networking
    * [Unifi Gateway Router](https://amzn.to/4fx7XqT) - Router and Wifi Access Point
    * [8-port POE Switch](https://amzn.to/3CxNhRc) - POE for Camera and ethernet ports for machines
    * [GLI-Net](https://amzn.to/3ANxxJm)<sup>*</sup> - Used to convert SSID to LAN. May not be needed, but has been super handy
- [ ] [Mini Tool Set](https://amzn.to/4fIqTmq)<sup>*</sup> - Good to have for incidental use
- [ ] [6" Scissors](https://amzn.to/4fCaIr5) - Lots of things to cut in the setup, always good to have a pair
- [ ] Silicone Ethernet Dust Cover - Why not have style while protecting ethernet ports?<sup>*</sup>
    * [Blue](https://amzn.to/3YVjg55)
    * [Red](https://amzn.to/3Czl0K3)
    * [Green](https://amzn.to/3ObXaGz)
    * [Yellow](https://amzn.to/3UVBi68)
- [ ] [Travel Case for Cables](https://amzn.to/3Z0HwTM)<sup>*</sup> - Really nice case to hold cables, power and tools
- [ ] [Travel Case Machines](https://amzn.to/4fV2dax) - Really nice case to hold cluster, power units, camera, player monitor and networking

<sup>*</sup> Optional or asthetic use

## Game Area
<a name="game-computer"></a>
- [ ] Game computer - Laptop, Chromebook, or mini-PC that hosts the Chrome browser
    * [Lenovo Chromebox](https://amzn.to/3ZllNr3) - Model used at conferences
    * [Intel NUC 13](https://amzn.to/3Z8vPvs) - A bit more performant allowing faster Inference FPS
- [ ] Camera Rig - Stable rig to hold the camera and lights
    * [NEEWER Overhead Camera Rig Small(er)](https://amzn.to/3ObeqM6) (28" wide)
    * [NEEWER Overahed Camrea Rig Large](https://amzn.to/3YTuvLF) (40" wide, preferred for more space)
- [ ] Tape
    * [Pinstripe Tape](https://amzn.to/3UWBQZo)<sup>*</sup> - Outline camera viewing area
    * [Clear double sided thick tape](https://amzn.to/4fT2Px4) - Tape tray down and adhere power to light rig. Easy to remove, stays in place
- [ ] [Battery Adjustable Lights](https://amzn.to/4fsH6fG)
- [ ] [Adjustable Ball-head Mount](https://amzn.to/4hVkRk2) - 2-3x needed to attach and adjust lights  camera
- [ ] [3" Slim Run Cat6a cable (10-pack)](https://amzn.to/3ObfvUv) - Best cables to connect hosts and game computer
- [ ] [10' Slim Run Cat6a cable (1-pack)](https://amzn.to/49bu6ch) - 10' cable for camera (adjust as needed)
- [ ] [HDMI Splitter](https://amzn.to/4fxONkK) - Split game machine output to player screen and audience screen
- [ ] [Flexible 22" arm w/ Clamp](https://amzn.to/3AJmYXG) - Hold the player monitor
- [ ] Power Adapter - Lights and Player Monitor power (choose one of the two)
    * [3 USB-A, 1 USB-C, 2 Plugs](https://amzn.to/40Yn9J8)
    * [4 USB-A, 4 USB-C](https://amzn.to/3Zc1QTt)
- [ ] [Reusable cable ties](https://amzn.to/4fSbVKB)
- [ ] [USB-C extender](https://amzn.to/3YQVsiR)<sup>*</sup> - Extend to power furthest light
- [ ] [Camrea Screw Set](https://amzn.to/3OdbYVv) - Set of mounting bits for lights and camrea

<sup>*</sup> Optional or asthetic use

## Data Sets

### Common Items

Regardless of the dataset chosen, these items are necessary

- [ ] [Model weight strips](https://amzn.to/4eBzSVm) - Add weight to items making them similar to a real product
- [ ] [4-piece tray](https://amzn.to/4fwxwbR) - All models have been trained on top of these trays

### gCafe

A set of items that represent a bakery or cafe shop

- [ ] [Macaroons](https://amzn.to/3ZdEtZH)
- [ ] [Square Cakes](https://amzn.to/40YgDCa)
- [ ] [Large Fancy Cakes](https://amzn.to/3YVj5Xt)
- [ ] [Swiss Rolls](https://amzn.to/40TcMXm)
- [ ] [Fancy Cupcakes](https://amzn.to/3ZaGNAr)
- [ ] [Fruit Cupcakes](https://amzn.to/40PYG8S)

### gTacos

Items that you might find at a taco shop or stand

- [ ] [French Fries](https://amzn.to/40Q4q2q)
- [ ] [Chicken Nuggets](https://amzn.to/48T46BY)
- [ ] [Hashbrowns](https://amzn.to/3ZlKZO9)
- [ ] [Fake Burgers](https://amzn.to/4hUsUhi)
- [ ] [Carne Wrappers](https://amzn.to/4hUt08E)
- [ ] [Pork Wrappers](https://amzn.to/4hUt3kQ)
- [ ] [Cheese Wrappers](https://amzn.to/4hRrqUY)
- [ ] [Food Boats](https://amzn.to/4926xT6)
- [ ] [Corn Ribs](https://amzn.to/3UVGvec)
- [ ] [Fake Tacos](https://amzn.to/3AOAxoK)
- [ ] [Chicken Wings](https://amzn.to/4eIl0F2)
- [ ] [Mini Doughnuts](https://amzn.to/3CH9fB6)

### Sushi Town

Set of items that make up a sushi platter

- [ ] [Bamboo Serving Platter](https://amzn.to/3YRGUQe)
- [ ] [6 piece sushi](https://amzn.to/4926BCk)
- [ ] [20 piece sushi](https://amzn.to/4fzzHeO)
- [ ] [10 piece sushi](https://amzn.to/3UXB1zM)
