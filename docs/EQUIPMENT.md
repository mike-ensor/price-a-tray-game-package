# Overview

Price-a-Tray can run on a variety of Kubernetes clusters, but was designed to work
on GDC Connected Server and Software Only. Public images from some of our events can be
found [https://bit.ly/price-a-tray-photos](https://bit.ly/price-a-tray-photos).

## GDC Connected Software Only

![GDC Connected Software Only](gdc-connected-software-only.jpg)

## GDC Connected Server

![GDC Connected Server](gdc-connected-server.jpg)

![GDC Connected Server](gdc-connected-server-no-case.jpg)